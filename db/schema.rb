# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140111192707) do

  create_table "definitions", force: true do |t|
    t.text     "text"
    t.integer  "word_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "image_files", force: true do |t|
    t.string   "filename"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "original_filename"
  end

  create_table "kanjis", force: true do |t|
    t.string   "character"
    t.integer  "strokes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "stroke_data"
  end

  create_table "kunyomis", force: true do |t|
    t.string   "text"
    t.integer  "kanji_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "meanings", force: true do |t|
    t.string   "text"
    t.integer  "kanji_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "nanoris", force: true do |t|
    t.string   "text"
    t.integer  "kanji_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "onyomis", force: true do |t|
    t.string   "text"
    t.integer  "kanji_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "photos", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.integer  "user_id"
  end

  create_table "sentences", force: true do |t|
    t.text     "english"
    t.text     "japanese"
    t.text     "japanese_raw"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_words", force: true do |t|
    t.integer  "user_id"
    t.integer  "word_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "words", force: true do |t|
    t.string   "kanji"
    t.string   "kana"
    t.boolean  "common"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
