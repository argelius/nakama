class CreateOnyomis < ActiveRecord::Migration
  def change
    create_table :onyomis do |t|
      t.string :text
      t.integer :kanji_id

      t.timestamps
    end
  end
end
