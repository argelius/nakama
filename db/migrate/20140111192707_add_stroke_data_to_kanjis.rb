class AddStrokeDataToKanjis < ActiveRecord::Migration
  def change
    add_column :kanjis, :stroke_data, :text
  end
end
