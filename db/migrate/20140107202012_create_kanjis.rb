class CreateKanjis < ActiveRecord::Migration
  def change
    create_table :kanjis do |t|
      t.string :character
      t.integer :strokes

      t.timestamps
    end
  end
end
