class CreateMeanings < ActiveRecord::Migration
  def change
    create_table :meanings do |t|
      t.string :text
      t.integer :kanji_id

      t.timestamps
    end
  end
end
