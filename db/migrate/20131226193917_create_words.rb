class CreateWords < ActiveRecord::Migration
  def change
    create_table :words do |t|
      t.string :kanji
      t.string :kana
      t.boolean :common

      t.timestamps
    end
  end
end
