class CreateSentences < ActiveRecord::Migration
  def change
    create_table :sentences do |t|
      t.text :english
      t.text :japanese
      t.text :japanese_raw

      t.timestamps
    end
  end
end
