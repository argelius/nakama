class CreateKunyomis < ActiveRecord::Migration
  def change
    create_table :kunyomis do |t|
      t.string :text
      t.integer :kanji_id

      t.timestamps
    end
  end
end
