class CreateNanoris < ActiveRecord::Migration
  def change
    create_table :nanoris do |t|
      t.string :text
      t.integer :kanji_id

      t.timestamps
    end
  end
end
