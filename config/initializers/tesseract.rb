require 'tesseract'
require 'tempfile'

TESSERACT_ENGINE = Tesseract::Engine.new {|e|
    e.language  = :jpn
}

cfg = Tempfile.new 'cfg'

cfg.write %{
chop_enable                         T
use_new_state_cost                  F
segment_segcost_rating              F
enable_new_segsearch                0
language_model_ngram_on             0
textord_force_make_prop_words       F
edges_max_children_per_outline      40
}

cfg.close
TESSERACT_ENGINE.load_config cfg.path
