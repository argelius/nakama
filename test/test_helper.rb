ENV["RAILS_ENV"] ||= "test"

require 'simplecov'
SimpleCov.start do
end

require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
    ActiveRecord::Migration.check_pending!

    # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
    #
    # Note: You'll currently still have to declare fixtures explicitly in integration tests
    # -- they do not yet inherit this setting
    fixtures :all

    # Add more helper methods to be used by all tests here...

    def login(email='user@domain.com', password='abc123')
        old_controller = @controller
        @controller = SessionsController.new

        post :create, :email => email, :password => password, :password_confirmation => password
        assert_redirected_to root_url

        assert_not_nil session[:user_id]
        assert_equal session[:user_id], User.find_by_email(email).id

        @controller = old_controller
    end
end
