require 'test_helper'

class DefinitionTest < ActiveSupport::TestCase
    test "should not save without text" do
        definition = Definition.new
        assert !definition.save, "saved without text"
    end
end
