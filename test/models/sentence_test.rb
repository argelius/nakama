# encoding: UTF-8

require 'test_helper'

class SentenceTest < ActiveSupport::TestCase
    params = {:english => "Hello", :japanese => "こんにちは", :japanese_raw => "こんにちは"}
    
    test "should be able to create with all fields set" do
        sentence = Sentence.new(params)
        assert sentence.save, "couldn't create sentence"
    end

    test "should not be created without english" do
        sentence = Sentence.new(params.except(:english))
        assert !sentence.save, "created sentence without english"
    end

    test "should not be created without japanese" do
        sentence = Sentence.new(params.except(:japanese))
        assert !sentence.save, "created sentence without japanese"
    end

    test "should not be created without japanese_raw" do
        sentence = Sentence.new(params.except(:japanese_raw))
        assert !sentence.save, "created sentence without japanese_raw"
    end
end
