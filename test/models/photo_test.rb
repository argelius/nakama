require 'test_helper'

class PhotoTest < ActiveSupport::TestCase
    test "file should not be created without attachment" do
        photo = Photo.new
        assert !photo.save, "photo object created without attachment."
    end
end
