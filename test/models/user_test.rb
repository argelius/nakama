require 'test_helper'

class UserTest < ActiveSupport::TestCase
    pw = "abc123"
    email = "a@a.com"

    test "invalid email" do
        user = User.new(:password => pw, 
                        :password_confirmation => pw, 
                        :email => "invalid")

        assert !user.save, "saved user with invalid email"
    end

    test "password confirmation" do
        user = User.new(:password => pw, 
                        :password_confirmation => "something else", 
                        :email => email)
    end

    test "downcase email" do
        user = User.new(:password => pw,
                        :password_confirmation => pw,
                        :email => "A@A.com")

        assert user.save, "should be able to save"
        assert user.email == "a@a.com"
    end

    test "unique email" do
        user = User.new(:password => pw,
                        :password_confirmation => pw,
                        :email => "a@a.com")
        user.save
        user = User.new(:password => pw,
                        :password_confirmation => pw,
                        :email => "a@a.com")

        assert !user.save, "email must be unique"
    end
end
