#encoding: utf-8

require 'test_helper'

class WordTest < ActiveSupport::TestCase
    test "must have kanji" do
        word = Word.new(:kana => "a")
        assert !word.save, "saved without kanji"
    end

    test "searching" do
        assert Word.search("本").length == 3, "should return three"
        assert Word.search("ほんとう").length == 1, "should only return one"
        assert Word.search("本").first == words(:one), "should return most common first."
        assert Word.search("book").length == 1, "searching for english didn't work."
        assert Word.search("本*").length == 2, "searching with wildcard didn't work."
    end

    test "check overriden to_param" do
        assert_match /-/, words(:anshin).to_param
        assert_no_match /-/, words(:one).to_param
    end
end
