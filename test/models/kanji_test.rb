# encoding: utf-8

require 'test_helper'

class KanjiTest < ActiveSupport::TestCase
    test "override to_param" do
        assert_equal kanjis(:hon).to_param(), "本"   
    end
end
