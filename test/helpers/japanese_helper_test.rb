# encoding: utf-8

require 'test_helper'

class JapaneseHelperTest < ActionView::TestCase
    test "katakana" do
        assert katakana? "カタカナ"
        assert_not katakana? "ひらがな"
        assert_not katakana? "漢字"
        assert_not katakana? "びんビール"
    end

    test "hiragana" do
        assert hiragana? "ひらがな"
        assert_not hiragana? "カタカナ"
        assert_not hiragana? "漢字"
        assert_not hiragana? "びんビール"
    end

    test "kana" do
        assert kana? "ひらがな"
        assert kana? "カタカナ"
        assert kana? "びんビール"
        assert_not kana? "漢字"
        assert_not kana? "スウェーデン人"
    end
end
