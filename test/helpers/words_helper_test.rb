# encoding: utf-8

require 'test_helper'

class WordsHelperTest < ActionView::TestCase
    test "verb forms" do
        assert_equal verb_forms('しまう').length, 7
        assert_equal verb_forms('しる').length, 10
        assert_equal verb_forms('かつ').length, 7
        assert_equal verb_forms('はなす').length, 7
        assert_equal verb_forms('ぬぐ').length, 7
        assert_equal verb_forms('きく').length, 7
        assert_equal verb_forms('しぬ').length, 7
        assert_equal verb_forms('よむ').length, 7
    end
end
