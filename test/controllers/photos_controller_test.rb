require 'test_helper'

class PhotosControllerTest < ActionController::TestCase
    test "redirect to login if not signed in" do
        get :index
        assert_redirected_to login_path
    end

    test "show users' photos when logged in" do
        login
        get :index
        assert_template :index
    end

    test "upload without image should return to index" do
        post :create, :photo => {:file => {}}
        assert_template "nakama/index"
    end

    test "get form" do
        get :new
        assert_template :new
    end

    test "upload image when not logged in" do
        file = fixture_file_upload "sample.jpg", "image/jpeg"
        assert_difference "Photo.count" do
            post :create, :photo => {:file => file}
        end
        photo = Photo.last
        assert_nil photo.user_id, "photo should not have a user id"
    end

    test "upload image when logged in" do
        login
        file = fixture_file_upload "sample.jpg", "image/jpeg"
        assert_difference "Photo.count" do
            post :create, :photo => {:file => file}
        end
        photo = Photo.last 
        assert_equal photo.user_id, users(:user).id, "photo doesn't belong to current user."
    end

    test "user shouldn't be able to view other users' photos" do
        get :show, {:id => photos(:two).id}
        assert_redirected_to login_path

        login

        get :show, {:id => photos(:two).id}
        assert_response :success
    end

    test "a user that is not logged in should only be able to see own pictures." do
        file = fixture_file_upload "sample.jpg", "image/jpeg"
        post :create, :photo => {:file => file}
        photo = Photo.last

        get :show, {:id => photo.id}
        assert_template :show

        get :show, {:id => photos(:one).id}
        assert_redirected_to root_url
    end

    test "shouldn't be able to remove other users' photos" do
        post :destroy, {:id => photos(:one).id}
        assert_redirected_to login_path
        
        post :destroy, {:id => photos(:two).id}
        assert_redirected_to login_path

        login

        assert_difference "Photo.count", -1 do
            post :destroy, {:id => photos(:two).id}
            assert_redirected_to myimages_path
        end
    end
end
