# encoding: utf-8

require 'test_helper'
require 'base64'

class KanjiControllerTest < ActionController::TestCase
    test "ocr" do
        post :find_character, {:data => "not an image"}
        assert_response :bad_request, "should not work"

        File.open(Rails.root.join('public', 'images', 'test.jpg')) do |f|
            data = Base64.encode64(f.read)
            post :find_character, {:data => data}
            assert_response :success, "unable to read image"
            json = ActiveSupport::JSON.decode @response.body
            assert_equal json["characters"], "未納者", "ocr doesn't work"

            f.close
        end
    end
end
