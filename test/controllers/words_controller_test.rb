# encoding: utf-8

require "test_helper"

class WordsControllerTest < ActionController::TestCase
    test "search for something that exists" do
        get :search, :q => "本"
        assert_response :success
        json = ActiveSupport::JSON.decode @response.body
        assert_equal json["word"]["kanji"], "本当"
    end

    test "search for something that doesn't exist" do
        get :search, :q => "blabla"
        assert_response :not_found
    end

    test "search empty" do
        get :search, :q => ""
        assert_response :success
    end

    test "show index" do
        get :index
        assert_template :index
    end

    test "search for a word" do
        get :index, :q => "本"
        assert_template :index
        assert_match @response.body, "本当"
        assert_no_match @response.body, "安心"
    end

    test "search for an empty string" do
        get :index, :q => ""
        assert_template :index
        assert_match @response.body, "本当"
        assert_match @response.body, "安心"

    test "show word" do
        get :show, :id => words(:one).kanji
        assert_template :show
    end

    test "show word by id" do
        get :show, :id => words(:one).id
        assert_template :show
    end

    test "show word where kanji representation is not unique" do
        get :show, :id => "#{words(:anshin).kanji}-#{words(:anshin).id}"
    end

    test "glossary" do
        get :glossary
        assert_no_match /ほんとう/, response.body
        assert_template :glossary

        get :glossary, :c => "ほ"
        assert_match /ほんとう/, response.body
        assert_template :glossary
    end

    test "get random" do
        @request.env["HTTP_ACCEPT"] = "application/json"
        get :random
        assert_template :random
    end
end
