require 'test_helper'

class KanjisControllerTest < ActionController::TestCase
    test "get both html and svg" do 
        get :show, {:id => kanjis(:hon).character}
        assert_template :show

        @request.env["HTTP_ACCEPT"] = "image/svg+xml"
        get :show, {:id => kanjis(:hon).character}
        assert_template :show
    end

    test "get random" do
        @request.env["HTTP_ACCEPT"] = "application/json"
        get :random
        assert_template :random
    end
end
