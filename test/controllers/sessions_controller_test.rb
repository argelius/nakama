require 'test_helper'

class SessionsControllerTest < ActionController::TestCase
    test "login with correct credentials" do
        post :create, {:email => 'user@domain.com', :password => 'abc123'}
        assert_redirected_to root_url
        assert_equal users(:user).id, session[:user_id], "not logged in"
    end

    test "login with incorrect password" do
        post :create, {:email => 'user@user.com', :password => 'wrong pass'}
        assert_template :new, "didn't render the correct page." 
    end

    test "login with incorrect email" do
        post :create, {:email => 'user2@domain.com', :password => 'wrong pass'}
        assert_template :new, "didn't render the correct page." 
    end

    test "logout" do
        post :create, {:email => 'user@domain.com', :password => 'abc123'}
        
        assert_not_nil session[:user_id], "didn't log in"
        get :destroy
        assert_nil session[:user_id], "didn't log out"
        assert_redirected_to root_url
    end
end
