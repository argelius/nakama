require "test_helper"

class UsersControllerTest < ActionController::TestCase
    test "show form" do
        get :new
        assert_template :new
    end
    
    test "invalid email" do
        assert_no_difference "User.count" do
            post :create, :user => {:email => "invalid", :password => "a", :password_confirmation => "a"}
            assert_template :new
        end
    end

    test "no password" do
        assert_no_difference "User.count" do
            post :create, :user => {:email => "abc@abc.com", :password => "", :password_confirmation => ""}
            assert_template :new
        end
    end

    test "different password" do
        assert_no_difference "User.count" do
            post :create, :user => {:email => "abc@abc.com", :password => "abc", :password_confirmation => "abcd"}
            assert_template :new
        end
    end

    test "existing email" do
        assert_no_difference "User.count" do
            post :create, :user => {:email => "user@domain.com", :password => "abc", :password_confirmation => "abc"}
            assert_template :new
        end
    end

    test "create user" do
        assert_difference "User.count" do
            post :create, :user => {:email => "otheruser@domain.com", :password => "abc", :password_confirmation => "abc"}
            assert_redirected_to root_path
        end
    end
end
