require 'test_helper'

class UserWordsControllerTest < ActionController::TestCase
    test "redirect to login if not logged in" do
        get :index
        assert_redirected_to login_path
    end

    test "show words if logged in" do
        login
        get :index
        assert_template :index
    end

    test "adding existing word should not increase count" do
        login
      
        assert_no_difference "UserWord.count" do
            post :create, {:id => user_words(:one).word_id}
            assert_response :found
        end
    end

    test "adding new word should increase count" do
        login

        assert_difference "UserWord.count" do
            post :create, {:id => words(:two).id}
            assert_response :found
        end
    end

    test "remove word" do
        login

        assert_difference "UserWord.count", -1 do
            post :destroy, {:id => users(:user).user_words.first.id}
            assert_response :found
        end
    end
end
