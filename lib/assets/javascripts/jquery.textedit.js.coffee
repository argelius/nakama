#
# jQuery plugin that makes text in a span editable.
#
# Andreas Argelius 2013
#

$ ->
    $.fn.textEdit = (options) ->
        options = $.extend
            select: ->
            deselect: ->
            change: ->
        , options || {}

        @set = (text) =>
            options.change.call this, text
            this.children().remove()
            $.each text.split(''), (k,v) =>
                $("<span>#{v}</span>").
                    appendTo(this).
                    css(cursor: "pointer").
                    clickToggle ->
                        options.select.call this
                        $(this).css color: "#8c001a"
                    , ->
                        options.deselect.call this
                        $(this).css color: ""
        this
