#
# jQuery plugin used to find characters in an image of text.
#
# Andreas Argelius 2013
#

$ ->
    class NakamaImageGray
        constructor: (width, height) ->
            @width = width
            @height = height
            @data = new Uint8ClampedArray width*height

        boxSum: (x, y, width, height) ->
            sum = 0
            for i in [y...height+y]
                for j in [x...width+x]
                    sum+=@data[i*@width+j]
            sum
   
        median: ->
            a = Array @data.length
            a[idx]=@data[idx] for idx in [0...@data.length]
            a.sort()
            a[Math.round @data.length/2]

        findBox: (x, y, width, height, sum) ->
            if width > 500 or height > 500
                return null
            
            left  = @boxSum x-1, y,   width+1, height
            right = @boxSum x,   y,   width+1, height
            up    = @boxSum x,   y-1, width,   height+1
            down  = @boxSum x,   y,   width,   height+1

            max = Math.max left, right, up, down

            if max-sum < 2
                if width+x > @width
                    width -= width+x-@width
                if height+y > @height
                    height -= height+y-@height
                if x < 0
                    width += x
                    x = 0
                if y < 0
                    height += y
                    y = 0

                return (
                    x: x
                    y: y
                    width: width
                    height: height
                )

            if max == left
                @findBox x-1, y,   width+1, height, left
            else if max == right
                @findBox x,   y,   width+1, height, right
            else if max == up
                @findBox x,   y-1, width,   height+1, up
            else if max == down
                @findBox x,   y,   width,   height+1, down

        fromRGB: (data) ->
            for idx in [0...data.length] by 4
                @data[idx/4] = Math.min(255,
                    Math.round 0.299*data[idx]+0.587*data[idx+1]+0.114*data[idx+2]
                )

        segment: ->
            median = (x, y, width, height) =>
                a = new Array(width*height)
                idx = 0
                for i in [y...Math.min(y+height, @height)]
                    for j in [x...Math.min(x+width, @width)]
                        a[idx++] = @data[i*@width+j]
                
                a.sort()
                a[Math.floor idx/2]

            threshold = (x, y, width, height, th) =>
                for i in [y...Math.min(y+height, @height)]
                    for j in [x...Math.min(x+width, @width)]
                        if @data[i*@width+j] < th
                            @data[i*@width+j] = 1
                        else
                            @data[i*@width+j] = 0
            
            size = 100
            for x in [0...@height] by size
                for y in [0...@width] by size
                    med = median x, y, size, size
                    threshold x, y, size, size, 0.80*med
    
        dilate: ->
            src = @data
            dst = new Uint8ClampedArray(src.length)
            for i in [1..@height]
                for j in [1..@width]
                    c = 0
                    for ii in [-1..1]
                        for jj in [-1..1]
                            c|=src[(i+ii)*@width+j+jj]
                    dst[i*@width+j] = c
            @data = dst

        erode: ->
            src = @data
            dst = new Uint8ClampedArray(src.length)
            for i in [1..@height]
                for j in [1..@width]
                    c = 1
                    for ii in [-1..1]
                        for jj in [-1..1]
                            c&=src[(i+ii)*@width+j+jj]
                    dst[i*@width+j] = c
            @data = dst

        showImage: ->
            $canvas = $("<canvas>").attr('width', @width).attr('height', @height).appendTo("body")

            ctx = $canvas[0].getContext "2d"

            id = ctx.createImageData @width, @height

            for i in [0...@height]
                for j in [0...@width]
                    p = @data[i*@width+j]
                    for k in [0...3]
                        id.data[i*@width*4+j*4+k] = 255*p
                    id.data[i*@width*4+j*4+3] = 255

            ctx.putImageData id, 0, 0

    class NakamaImage
        constructor: ($image, sz) ->
            @image = $image
            @size = sz

        marginLeft: ->
            parseInt @image.css "margin-left"

        marginTop: ->
            parseInt @image.css "margin-top"

        width: ->
            @image.width()

        height: ->
            @image.height()

        zoom: (v) ->
            if v < 0 and @width() <= @size
                return

            sx = 1-(@marginLeft()+@width()-@size/2)/@width()
            sy = 1-(@marginTop()+@height()-@size/2)/@height()

            @image.animate
                width: "+=#{v}"
                marginLeft: "-=#{sx*v}"
                marginTop: "-=#{sy*v}"
            , 50

        pan: (x, y) ->
            @image.animate
                marginLeft: "+=#{x}"
                marginTop: "+=#{y}",
            , 50

    $.fn.kanjiFinder = ->
        that = this

        options = $.extend
            url: "/kanji/find"
            callback: ->
        , arguments[0] || {}

        image = new NakamaImage(this, @parent().width())

        @zoomIn = ->
            image.zoom 200
        @zoomOut = ->
            image.zoom -200
        @panLeft = ->
            image.pan 50, 0
        @panRight = ->
            image.pan -50, 0
        @panUp = ->
            image.pan 0, 50
        @panDown = ->
            image.pan 0, -50
        @blacklist = ""
        @mode = 5
      
        @parent().css
            overflow: "hidden"
            border: "1px solid #e7e7e7"
            backgroundColor: "#f8f8f8"
    
        fix = =>
            @width @parent().width()
            @parent().height @width()
            @css
                margin: 0

        do fix
        $(window).on 'resize orientationchange', fix
        
        # Get original image data.
        grayImage = null
        $("<img></img>").attr('src', @attr 'src').load ->
            w = @width
            h = @height
            $canvas = $("<canvas></canvas>").attr
                width: w
                height: h

            ctx = $canvas[0].getContext "2d"
            ctx.drawImage this, 0, 0

            grayImage = new NakamaImageGray w, h
            grayImage.fromRGB (ctx.getImageData 0, 0, w, h).data
            grayImage.segment()
            grayImage.dilate()
            grayImage.dilate()
            #grayImage.showImage() # Debugging

        @find = (x, y, width, height, callback) ->
            s = grayImage.width/that[0].width
            
            x = Math.round(s*x)
            y = Math.round(s*y)
            width = Math.round(s*width)
            height = Math.round(s*height)
            
            box = grayImage.findBox(x, y, width, height)

            if box?
                $("<img></img>").attr('src', $(this).attr 'src').load ->
                    w = box.width
                    h = box.height
                    $canvas = $("<canvas></canvas>").attr
                        width: w
                        height: h
                    
                    ctx = $canvas[0].getContext "2d"
                    ctx.drawImage this, box.x, box.y, w, h, 0, 0, w, h

                    dataURL = $canvas[0].toDataURL()

                    $.ajax options.url,
                        type: "POST"
                        data:
                            data: dataURL.replace(/^data:image\/(png|jpg);base64,/, "")
                            blacklist: that.blacklist
                            page_segmentation_mode: that.mode
                        success: (ret) ->
                            callback ret.characters, dataURL
                        error: ->
                            callback "", dataURL
            else
                callback ""
        this
