#
# jQuery plugin that makes image pannable both on mobile and desktop.
#

$ ->
    currentTime = ->
        (new Date()).getTime()

    $.fn.imagePan = ->
        options = $.extend
            click: ->,
            width: 500,
            height: 500
        , arguments[0] || {}

        that = this

        # Force image size
        this.attr
            width: this.width()
            height: this.height()
        .css
            width: this.width()
            height: this.height()
            maxWidth: this.width()
            maxHeight: this.height()

        $container = $("<div></div>").
            addClass("imagepan-container").
            css("overflow", "hidden").
            css("background-color", "#ddd").
            width(options.width).
            height(options.height)

        $container.on "dblclick doubletap", (e) ->
            alert "HAHHAHA"
            if that.css("zoom") > 1.0
                that.animate {zoom: 1.0}
            else
                that.animate
                    zoom: 4.0
                    marginLeft: "/=4"

        startTime = null
        $container.on 'mousedown touchstart', (e) ->
            e.preventDefault()
            startTime = currentTime()
            
            prev = e
            $(this).on 'mousemove touchmove', (e) ->
                e.preventDefault()

                if e.originalEvent.touches?
                    e = e.originalEvent.touches[0]
                else if e.originalEvent.changedTouches?
                    e = e.originalEvent.changedTouches[0]

                dx = (prev.pageX-e.pageX)
                dy = (prev.pageY-e.pageY)

                that.stop(true, true).animate
                    marginLeft: "-=#{dx/2}"
                    marginTop: "-=#{dy/2}"
                , 0

                prev = e

            $(this).on 'mouseup touchend', (e) ->
                if currentTime()-startTime < 200
                    if e.originalEvent.changedTouches?
                        e = e.originalEvent.changedTouches[0]

                    options.click.call that,
                        x: e.pageX-$(this).offset().left-parseInt(that.css "margin-left")
                        y: e.pageY-$(this).offset().top-parseInt(that.css "margin-top")

                prev = null
                that.stop(true, true)
                $(this).unbind 'mousemove mouseup touchmove touchup'

        this.wrap $container
