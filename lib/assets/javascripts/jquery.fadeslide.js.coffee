#
#   jQuery plugin that fades and slides element.
#

$ ->
    $.fn.fadeUp = (t1, t2) ->
        if not t1?
            t1 = 1000

        if not t2?
            t2 = t1/2
            t1 /= 2

        this.animate({opacity: 0.01}).slideUp()

    $.fn.fadeDown = (t1, t2) ->
        if not t1?
            t1 = 1000

        if not t2?
            t2 = t1/2
            t1 /= 2

        this.slideDown().animate({opacity: 1.0})
 
