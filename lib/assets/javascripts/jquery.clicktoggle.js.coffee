#
# jQuery plugin that mimics old jQuery toggle
#
# Andreas Argelius 2013
# 

$.fn.clickToggle = (a, b) ->
    toggled = 0
    this.each ->
        $(this).click ->
            toggled ^= 1
            if toggled
                return a.apply this, arguments
            else
                return b.apply this, arguments
