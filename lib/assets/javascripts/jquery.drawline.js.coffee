#
# jQuery plugin that creates a transparent canvas overlay for an image that is drawable.
#

$ ->
    $.fn.drawLine = ->
        $overlay = $("<canvas></canvas>")
            .css
                position: "absolute"
                top: "0px"
                left: "0px"
                width: "100%"
                height: "100%"
                zIndex: 10

        ctx = $overlay[0].getContext "2d"
        ctx.lineWidth = 10
        ctx.strokeStyle = "rgba(0,255,0,0.5)"

        getPos = (e) ->
            if e.type.indexOf("touch") == 0
                e = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0]

            x = e.pageX-$overlay.offset().left
            y = e.pageY-$overlay.offset().top
            sx = $overlay[0].width/$overlay.width()
            sy = $overlay[0].height/$overlay.height()

            return {
                x: sx*x,
                y: sy*y
            }

        start = null
        $overlay.on 'mousedown touchstart', (e) =>
            start = getPos e
            $overlay.addClass "drawing"

        $overlay.on 'mouseup touchend', ->
            ctx.clearRect 0, 0, $overlay[0].width, $overlay[0].height
            $overlay.removeClass "drawing"

        $overlay.on 'mousedown touchstart mouseup touchend', (e) =>
            @trigger e

        $overlay.on 'mousemove touchmove', (e) ->
            if $(this).hasClass "drawing"
                ctx.clearRect 0, 0, $overlay[0].width, $overlay[0].height
                
                end = getPos e
               
                ctx.beginPath()
                ctx.moveTo start.x, start.y
                ctx.lineTo end.x, end.y
                ctx.stroke()
                ctx.closePath()

        this.after $overlay
