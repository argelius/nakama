namespace :sentences do
    desc "Download Tanaka corpus."
    task :download do
        `wget -N http://www.csse.monash.edu.au/~jwb/examples.utf.gz`
    end

    desc "Extract Tanaka corpus."
    task :extract => [:download] do
        `gunzip -c examples.utf.gz > examples.utf`
    end

    desc "Remove all sentences from database."
    task :empty => :environment do
        puts "Removing sentences."
        Sentence.delete_all
    end

    desc "Import sentences to database."
    task :import => [:environment, :extract, :empty] do
        puts "Adding sentences to database."
        sentences = []
        number_of_lines = `wc -l examples.utf`.to_i
        
        File.open('examples.utf', 'r').lines.each_slice(2) do |pair|
            params = {}

            pair[0] =~ /A: (.*)\t(.*)#ID=/
            params[:japanese] = $1
            params[:english] = $2
            
            pair[1] =~ /B: (.*)$/
            params[:japanese_raw] = $1

            sentences << params

            if $.%1000 == 0 
                print "\r#{100*$./number_of_lines}%"

                ActiveRecord::Base.transaction do
                    Sentence.create sentences
                end

                sentences = []
            end
        end
    end
end
