namespace :dictionary do
    desc "Download EDICT file."
    task :download do
        `wget -N http://ftp.monash.edu.au/pub/nihongo/edict.gz`
    end

    desc "Extract EDICT file."
    task :extract => [:download] do
        `gunzip -c edict.gz > edict` 
    end

    desc "Remove all entries from database."
    task :empty => :environment do
        puts "Removing word definitions."
        Definition.delete_all
        puts "Removing words."
        Word.delete_all
    end

    desc "Import dictionary to database."
    task :import => [:environment, :extract, :empty] do
        def parse_line(l)
            if l =~ /\/\(P\)\/$/
                common = true
                l.sub! '/(P)/', ''
            else
                common = false
            end

            k = l.split('/')[0].strip

            if k =~ /^(.*) (\[(.*)\])?/
                kanji = $1
                kana = $3
            else
                kanji = k.strip
                kana = ""
            end

            definitions = []
            definition = ""
            l.split('/')[1..-1].each do |d|
                d.strip!
                if d =~ /\([0-9]{1,2}\)/
                    if definition.length > 0
                        definitions << Definition.new(:text => definition.sub(/; $/, ""))
                    end
                    definition = ""
                end

                definition += "#{d.sub(/\([0-9]{1,2}\)/, "")}; "
            end
            
            if definition.length > 0
                definitions << Definition.new(:text => definition.sub(/; $/, ""))
            end

            return {:kanji => kanji, :kana => kana, :definitions => definitions, :common => common}
        end
        
        puts "Adding words to database."
        words = []
        number_of_lines = `wc -l edict`.to_i

        File.open('edict', 'r').each do |l|
            l = l.encode(Encoding::UTF_8, Encoding::EUCJP_MS).strip
            params = parse_line(l)

            words << params

            if $.%1000 == 0
                print "\r#{100*$./number_of_lines}%"

                ActiveRecord::Base.transaction do
                    Word.create words
                end

                words = []
            end
        end
        puts ""
        puts "Import finished. You have a fresh dictionary!"
    end
end
