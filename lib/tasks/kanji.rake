require "#{Rails.root}/app/helpers/japanese_helper"
include JapaneseHelper

require 'nokogiri'

namespace :kanji do
    desc "Download KANJIDIC and kanjivg file."
    task :download do
        `wget -N http://ftp.monash.edu.au/pub/nihongo/kanjidic.gz`
        `wget -N https://github.com/KanjiVG/kanjivg/releases/download/r20130901/kanjivg-20130901.xml.gz`
    end

    desc "Extract files."
    task :extract => [:download] do
        `gunzip -c kanjidic.gz > kanjidic` 
        `gunzip -c kanjivg-20130901.xml.gz > kanjivg.xml`
    end

    desc "Remove all entries from database."
    task :empty => :environment do
        puts "Removing kanjis."
        Kanji.delete_all
    end

    desc "Import kanji to database."
    task :import_kanji => [:environment, :extract, :empty] do
        puts "Adding kanji to database."
        
        def parse_line(l)
            kanji = l[0]

            onyomi = []
            kunyomi = []
            nanori = []
            meanings = []
            strokes = 0

            names = false
            l.split(" ").each do |w|
                w.strip!

                if w =~ /^S([0-9]+)$/
                    strokes = $1.to_i
                end

                if w =~ /^T[0-9]+$/
                    names = true
                end

                if hiragana? w.gsub /\./, ""
                    if names
                        nanori << Nanori.new(:text => w)
                    else
                        kunyomi << Kunyomi.new(:text => w)
                    end
                end

                if katakana? w
                    onyomi << Onyomi.new(:text => w)
                end
            end

            l.scan(/\{.+?\}/) do |w|
                meanings << Meaning.new(:text => w.gsub(/[{}]/, ""))
            end

            {:character => kanji, :onyomis => onyomi, :kunyomis => kunyomi, :strokes => strokes, :nanoris => nanori, :meanings => meanings}
        end

        kanjis = []
        number_of_lines = `wc -l kanjidic`.to_i

        File.open('kanjidic', 'r').each do |l|
            if l[0] == "#"
                next
            end

            print "\r#{100*$./number_of_lines}%"


            l = l.encode(Encoding::UTF_8, Encoding::EUCJP_MS).strip

            kanjis << parse_line(l)

            if $. % 100 == 0
                print "\r#{100*$./number_of_lines}%"

                ActiveRecord::Base.transaction do
                    Kanji.create kanjis 
                end

                kanjis = []
            end
        end

        ActiveRecord::Base.transaction do
            Kanji.create kanjis
        end

        puts "\nFinished importing kanji!"
    end

    desc "Import kanji strokes to database."
    task :import_kanji_strokes => [:environment, :import_kanji] do
        puts "Importing stroke data."
        kanjis = []

        Nokogiri::XML(File.open('kanjivg.xml')).css("kanji>g").each do |group|
            el = group.attr("kvg:element")
            if not el.nil?
                Kanji.where(:character => el).each do |kanji|
                    puts "Adding strokes for #{kanji.character}..."
                    kanji.stroke_data = []
                    group.css("path").each do |stroke|
                        kanji.stroke_data << stroke.attr("d")
                    end
                    kanjis << kanji
                end
            end

            if $. % 100 == 0
                ActiveRecord::Base.transaction do
                    kanjis.each do |kanji| 
                        kanji.save() 
                    end

                    kanjis = []
                end
            end
        end

        ActiveRecord::Base.transaction do
            kanjis.each do |kanji|
                kanji.save()
            end
        end
    end

    desc "Run imports."
    task :import => [:import_kanji_strokes]
end
