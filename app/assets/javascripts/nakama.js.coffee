# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "page:load ready", ->
    $("#new_photo button").click (e) ->
        e.preventDefault()

        $input = $(this).siblings("input")
        if $(this).attr("id") == "capture"
            $input.attr("capture", "camera").trigger "click"
        else
            $input.removeAttr("capture").trigger "click"

    $("#photo_file").change ->
        $("#new_photo").trigger "submit"
