$(document).on "page:load ready", ->
    $("#q").kana({mode: "hiragana"})

    $("#switch-input-mode").click ->
        modes = ['hiragana', 'romaji', 'katakana']

        if $(this).text() == "あ"
            $("#q").kana({mode: "romaji"})
            $(this).text("Ａ")
        else if $(this).text() == "Ａ"
            $("#q").kana({mode: "katakana"})
            $(this).text("ア")
        else
            $("#q").kana({mode: "hiragana"})
            $(this).text("あ")

        $("#q").focus()
