$(document).on 'page:load ready', ->
    $("input#capture").change ->
        if $(this).is(":checked")
            $("input#photo_file").attr "capture", "camera"
        else
            $("input#photo_file").removeAttr "capture"
    

    $("#nakama-image").load ->
        $(this).drawLine()

        kanjiFinder = $(this).kanjiFinder
            url: '/kanji/find'

        textEdit = $("#nakama-kanji").textEdit
            select: ->
                kanjiFinder.blacklist += $(this).text()
            deselect: ->
                kanjiFinder.blacklist.replace($(this).text(), "")
            change: (text) ->
                $kanji = $("#nakama-result-kanji").text ""
                $kana = $("#nakama-result-kana").text ""
                $search = $("#nakama-result-search").attr "href", "/dictionary?q=#{text}"
                $definition = $("#nakama-result-definition").text ""

                $.ajax "/search?q=#{text}",
                    success: (res) ->
                        $kanji.html res.link
                        $kana.text res.word.kana
                        $definition.text res.definitions[0].text
                    error: ->
                        $kanji.text "No match found in dictionary."

        $(this).on 'mousedown touchstart', (e) ->
            e.preventDefault()

            if e.type == 'touchstart'
                e = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0]

            x = e.pageX-$(this).offset().left
            y = e.pageY-$(this).offset().top

            $(this).on 'mouseup touchend', (e) ->
                $(this).unbind 'mouseup touchend'

                if e.type == 'touchend'
                    e = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0]

                xNew = e.pageX-$(this).offset().left
                yNew = e.pageY-$(this).offset().top

                width = Math.abs(x-xNew)
                height = Math.abs(y-yNew)
                x = Math.min xNew, x
                y = Math.min yNew, y

                kanjiFinder.mode = if width > height then 6 else 5

                $("body").addClass "loading"
                kanjiFinder.blacklist = ""
                kanjiFinder.find x, y, width, height, (characters, dataURL) ->
                    $("body").removeClass "loading"
                    if characters.length
                        textEdit.set characters
                        
                        $("#nakama-modal").modal
                            backdrop: 'static'

                        $("#nakama-data-url").attr('href', dataURL)
                        $("#nakama-retry").unbind("click").click ->
                            $("body").addClass "loading"
                            kanjiFinder.find x, y, width, height, (characters) ->
                                $("body").removeClass "loading"
                                textEdit.set characters
                    else
                        $("#nakama-no-match-modal").modal
                            backdrop: 'static'

        $("#nakama-zoom-in").click ->
            kanjiFinder.zoomIn()
        $("#nakama-zoom-out").click ->
            kanjiFinder.zoomOut()
        $("#nakama-pan-left").click ->
            kanjiFinder.panLeft()
        $("#nakama-pan-right").click ->
            kanjiFinder.panRight()
        $("#nakama-pan-up").click ->
            kanjiFinder.panUp()
        $("#nakama-pan-down").click ->
            kanjiFinder.panDown()
