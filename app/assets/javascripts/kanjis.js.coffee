$(document).on "page:load ready", ->
    $kanjis = $("#kanji-stroke-order svg")
    $kanjis.hide().first().show()

    i = 0
    setInterval ->
        $kanjis.each (j) ->
            if j==i then $(this).show() else $(this).hide()

        i=(i+1)%$kanjis.length
    , 1000
