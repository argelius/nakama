require 'rasem'

class Kanji < ActiveRecord::Base
    def to_param
        character
    end

    serialize :stroke_data, Array
    has_many :meanings, dependent: :destroy
    has_many :onyomis, dependent: :destroy
    has_many :kunyomis, dependent: :destroy
    has_many :nanoris, dependent: :destroy
end
