class Photo < ActiveRecord::Base
    self.per_page = 6

    has_attached_file :file, :styles => { :small => "250x250#", :medium => "500x500#", :large => "1000x1000#" }

    validates_attachment_presence :file 
    validates_attachment_size :file, 
        :less_than => 10.megabytes
    validates_attachment_content_type :file,
        :content_type => ['image/jpeg', 'image/png']

    before_create :randomize_file_name

    belongs_to :user

    private

    def randomize_file_name
        extension = File.extname(file_file_name).downcase
        self.file.instance_write(:file_name, "#{SecureRandom.hex(16)}#{extension}")
    end

end
