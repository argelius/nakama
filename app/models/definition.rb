class Definition < ActiveRecord::Base
    belongs_to :word

    validates :text, presence: true
end
