class Sentence < ActiveRecord::Base
    self.per_page = 10

    validates_presence_of :english, :on => :create 
    validates_presence_of :japanese, :on => :create 
    validates_presence_of :japanese_raw, :on => :create 
end
