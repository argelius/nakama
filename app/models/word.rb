# encoding: utf-8

class Word < ActiveRecord::Base
    def to_param
        @words = Word.where(:kanji => kanji)

        if @words.count() == 1
            kanji
        else
            "#{kanji}-#{id}"
        end
    end
    
    self.per_page = 10
    
    has_many :definitions

    has_many :user_words
    has_many :users, :through => :user_words

    validates_presence_of :kanji, :on => :create

    def self.search(q)
        result = joins(:definitions)

        q.strip!
        if q.length > 0
            q.split(/[ 　]+/).each do |w|
                if w.include? "*"
                    patt = w.gsub "*", "%"
                else
                    patt = "%#{w}%"
                end
            
                if w.ascii_only?
                    result = result.where("LOWER(definitions.text) LIKE LOWER(?)", "%#{w}%")
                else
                    result = result.where("kanji LIKE ? or kana LIKE ?", patt, patt)
                end
            end
        end

        result.group("words.id").order("common DESC, length(kanji), kana")
    end
end
