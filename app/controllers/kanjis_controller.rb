class KanjisController < ApplicationController
    def show
        @kanji = Kanji.where(:character => params[:id]).
            includes(:meanings, :kunyomis, :onyomis, :nanoris).
            first

        respond_to do |format|
            format.html do
                @words = Word.where("kanji LIKE ?", "%#{params[:id]}%").
                    order("common DESC, LENGTH(kanji)").
                    includes(:definitions)
                render "show"
            end
            format.svg do
                render "show"
            end
        end
    end

    def random
        @kanji = Kanji.order("RANDOM()").
            includes(:onyomis, :kunyomis, :nanoris, :meanings).
            first

        render "random"
    end
end
