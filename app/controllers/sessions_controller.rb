class SessionsController < ApplicationController
    def new
    end

    def create
        user = User.find_by_email(params[:email].downcase)
        if user and user.authenticate(params[:password])
            session[:user_id] = user.id
            redirect_to root_url, :notice => "You have successfully logged in."
        else
            flash.now.alert = "Incorrect email or password. Please try again."
            render :new
        end
    end

    def destroy
        session[:user_id] = nil
        redirect_to root_url, :notice => "You have been logged out."
    end
end
