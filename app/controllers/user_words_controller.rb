class UserWordsController < ApplicationController
    before_action :require_login

    def index
        @user_words = current_user.user_words.paginate(:page => params[:page]).order("created_at DESC")
    end
    
    def create
        begin
            current_user.words.find(params[:id])
        rescue ActiveRecord::RecordNotFound
            @word = Word.find params[:id]
            current_user.words << @word
        else
            user_word = current_user.user_words.where("word_id = ?", params[:id]).first
            user_word.created_at = Time.now
            user_word.save
        end

        redirect_to mywords_path, :notice => "Word added."
    end

    def destroy
        @user_word = current_user.user_words.find params[:id]
        @user_word.destroy
        redirect_to mywords_path, :notice => "Word removed." 
    end

    private
    def require_login
        unless current_user
            redirect_to login_path, :notice => "Please sign in."
        end
    end
end
