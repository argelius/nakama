class PhotosController < ApplicationController
    def index
        if not current_user
            redirect_to login_path, :notice => "Please sign in."
        else
            @photos = current_user.photos.paginate(:page => params[:page])
        end
    end
    
    def new
        @photo = Photo.new
    end

    def create
        if current_user
            @photo = current_user.photos.build(photo_params)
        else
            @photo = Photo.new photo_params
        end

        if @photo.save
            if not session[:photo_ids].kind_of? Array
                session[:photo_ids] = []
            end

            session[:photo_ids] << @photo.id
            redirect_to @photo
        else
            flash.now.alert = "Unable to upload image!"
            render "nakama/index"
        end
    end
    
    def show
        @photo = Photo.find params[:id]
        
        if @photo.user
            if @photo.user != current_user
                redirect_to login_path, :notice => "Please sign in."
            end
        else
            if not session[:photo_ids].kind_of? Array or not session[:photo_ids].include? @photo.id
                redirect_to root_url, :notice => "You're not allowed to view this photo."
            end
        end
    end

    def destroy
        if current_user
            @photo = current_user.photos.find params[:id]
            @photo.destroy
            redirect_to myimages_path, :notice => "Image removed."
        else
            redirect_to login_path, :notice => "Please sign in."
        end
    end

    def sample
    end

    private
    def photo_params
        if params.has_key? :photo
            params.require(:photo).permit(:file)
        end
    end
end
