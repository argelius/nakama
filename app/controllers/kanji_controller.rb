require 'base64'

class KanjiController < ApplicationController
    def find_character
        defaults = {"language" => "jpn", "blacklist" => "", "page_segmentation_mode" => 5, "data" => ""}
        defaults.merge!(params)
        params = defaults
    
        e = TESSERACT_ENGINE
        e.language = params["language"]
        e.blacklist = params["blacklist"]
        e.page_segmentation_mode = params["page_segmentation_mode"].to_i

        data = Base64.decode64(params["data"])

        begin
            characters = e.text_for(data).gsub(/\s+/, "")
        rescue ArgumentError
            render json: { :result => "error", :message => "invalid image" }, status: :bad_request
        else
            render json: { :result => "success", :characters => characters }
        end
    end
end
