class WordsController < ApplicationController
    def index
        if params[:q].nil?
            @words = nil
        else
            @words = Word.search(params[:q]).
                paginate(:page => params[:page])
        end
    end

    def glossary
        c = params[:c]

        @words = Word.where("kana LIKE ?", "#{c}%")

        idx = if c.nil? then 1 else c.length+1 end
        @characters = @words.
            select("SUBSTR(kana, #{idx}, 1) AS character").
            group("character").
            order("character ASC")

        @words = @words.
            where("LENGTH(?) > 0", c).
            includes(:definitions).
            paginate(:page => params[:page])
    end

    def show
        if params[:id].include? '-'
            @word = Word.find(params[:id].split("-")[1])
        elsif params[:id] =~ /^[0-9]+$/
            @word = Word.find(params[:id])
        else
            @word = Word.where(:kanji => params[:id]).first
        end

        @kanjis = Kanji.where("character in (?)", @word.kanji.scan(/./))
        @sentences = Sentence.where("japanese_raw LIKE ?", "%#{@word.kanji}%").
            order("LENGTH(japanese) ASC").paginate(:page => params[:page])
    end

    def search
        @word = Word.search(params[:q] || "").first

        if params[:q].nil? or not @word
            render json: { :result => "error", :message => "no result" }, status: :not_found
        else
            render json: {
                :result => "success",
                :word => @word,
                :definitions => @word.definitions,
                :link => view_context.link_to(@word.kanji, @word)
            }
        end
    end

    def random
        @word = Word.
            where(:common => true).
            includes(:definitions).
            order("RANDOM()").
            first

        render "random"
    end
end
