# encoding: utf-8

module ApplicationHelper
    def navbar_link_to(body, icon, url)
        icon = "<i class='fa fa-#{icon}'></i>"
        "<li><a href='#{url}'>#{icon} #{body}</a></li>".html_safe
    end

    def title
        "Nakama"
    end
end
