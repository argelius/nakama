# encoding: utf-8

module JapaneseHelper
    def hiragana_char?(c)
        "\u{3041}" <= c and c <= "\u{3094}"
    end

    def katakana_char?(c)
        "\u{30a1}" <= c and c <= "\u{30fb}"
    end

    def kana_symbol?(c)
        c == "\u{309e}" or 
        c == "\u{309b}" or
        c == "\u{309c}" or
        c == "\u{30fc}" or
        c == "\u{ff70}"
    end

    def hiragana?(str)
        str.scan(/./).all? do |c|
            hiragana_char? c or kana_symbol? c
        end
    end

    def katakana?(str)
        str.scan(/./).all? do |c|
            katakana_char? c or kana_symbol? c
        end
    end

    def kana?(str)
        str.scan(/./).all? do |c|
            hiragana_char? c or katakana_char? c or kana_symbol? c
        end       
    end

    def katakana_to_hiragana(str)
        str.unpack("U*").map { |c|
            if c >= 0x30a1 and c <= 0x30fb
                c-96
            else
                c
            end
        }.pack("U*")
    end

    def hiragana_to_katakana(str)
        str.unpack("U*").map { |c|
            if c >= 0x3041 and c <= 0x3094
                c+96
            else
                c
            end
        }.pack("U*")
    end
end
