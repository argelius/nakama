# encoding: UTF-8

module WordsHelper
    def verb_forms(word)
        forms = [word]

        if word =~ /[うるつすぐくぬむ]$/
            case word[-1]
            when 'う'
                forms << word.sub(/う$/, "い")
                forms << word.sub(/う$/, "って")
                forms << word.sub(/う$/, "った")
                forms << word.sub(/う$/, "わ")
                forms << word.sub(/う$/, "お")
                forms << word.sub(/う$/, "え")
            when 'る'
                forms << word.sub(/る$/, "り")
                forms << word.sub(/る$/, "ら")
                forms << word.sub(/る$/, "って")
                forms << word.sub(/る$/, "た")
                forms << word.sub(/る$/, "て")
                forms << word.sub(/る$/, "ろ")
                forms << word.sub(/る$/, "よ")
                forms << word.sub(/る$/, "れ")
                forms << word.sub(/る$/, "")
            when 'つ'
                forms << word.sub(/つ$/, "ち")
                forms << word.sub(/つ$/, "た")
                forms << word.sub(/つ$/, "った")
                forms << word.sub(/つ$/, "って")
                forms << word.sub(/つ$/, "と")
                forms << word.sub(/つ$/, "て")
            when 'す'
                forms << word.sub(/す$/, "して")
                forms << word.sub(/す$/, "さ")
                forms << word.sub(/す$/, "した")
                forms << word.sub(/す$/, "し")
                forms << word.sub(/す$/, "そ")
                forms << word.sub(/す$/, "せ")
            when 'ぐ'
                forms << word.sub(/ぐ$/, "いだ")
                forms << word.sub(/ぐ$/, "いで")
                forms << word.sub(/ぐ$/, "ぎ")
                forms << word.sub(/ぐ$/, "が")
                forms << word.sub(/ぐ$/, "ご")
                forms << word.sub(/ぐ$/, "げ")
            when 'く'
                forms << word.sub(/く$/, "いた")
                forms << word.sub(/く$/, "いて")
                forms << word.sub(/く$/, "き")
                forms << word.sub(/く$/, "か")
                forms << word.sub(/く$/, "こ")
                forms << word.sub(/く$/, "け")
            when 'ぬ'
                forms << word.sub(/ぬ$/, "んだ")
                forms << word.sub(/ぬ$/, "んで")
                forms << word.sub(/ぬ$/, "に")
                forms << word.sub(/ぬ$/, "な")
                forms << word.sub(/ぬ$/, "の")
                forms << word.sub(/ぬ$/, "ね")
            when 'む'
                forms << word.sub(/む$/, "んだ")
                forms << word.sub(/む$/, "んで")
                forms << word.sub(/む$/, "み")
                forms << word.sub(/む$/, "ま")
                forms << word.sub(/む$/, "も")
                forms << word.sub(/む$/, "め")
            end

            
        end

        return forms
    end
end
